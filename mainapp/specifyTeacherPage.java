package mainapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import javax.swing.JOptionPane;


public class specifyTeacherPage extends javax.swing.JFrame {

    static HashMap<String,Teacher> teacher1_info = new HashMap<>();
    static HashMap<String,Teacher> teacher2_info = new HashMap<>();
    String teachers_id="";
    Teacher t2,t1;
    
    public specifyTeacherPage() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        student_name = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        guidteacherBox1 = new javax.swing.JComboBox<>();
        guidteacherBox2 = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        save = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("سامانه دفاعیه دانشگاهی");

        student_name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        student_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        student_name.setText("امبرمحمد کریمی");

        back.setText("بازگشت");
        back.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backMouseClicked(evt);
            }
        });

        guidteacherBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        guidteacherBox1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                guidteacherBox1MouseClicked(evt);
            }
        });

        guidteacherBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        guidteacherBox2.setEnabled(false);
        guidteacherBox2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                guidteacherBox2MouseClicked(evt);
            }
        });

        jLabel2.setText("استاد راهنما 2:");

        jLabel3.setText("استاد راهنما 1:");

        save.setText("ثبت");
        save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                saveMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(32, 32, 32)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(back)
                        .addGap(337, 337, 337)
                        .addComponent(student_name, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(24, 24, 24))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(save, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(guidteacherBox2, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(guidteacherBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(69, 69, 69)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))
                        .addGap(94, 94, 94))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(back)
                    .addComponent(student_name))
                .addGap(67, 67, 67)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guidteacherBox1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3))
                .addGap(62, 62, 62)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guidteacherBox2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addGap(125, 125, 125)
                .addComponent(save)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backMouseClicked
        // TODO add your handling code here:
        this.dispose();
        new studentPage().setVisible(true);
    }//GEN-LAST:event_backMouseClicked

    private void saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_saveMouseClicked
        // TODO add your handling code here:
        this.dispose();
        new studentPage().setVisible(true);
        
    }//GEN-LAST:event_saveMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        save.setEnabled(false);
        guidteacherBox1.setSelectedItem(null);
        guidteacherBox2.setSelectedItem(null);
        try{
            Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2=stmt2.executeQuery("select * from teachers");
                
                while (res2.next()) {                    
                    Teacher teacher = new Teacher();
                    teacher.name = res2.getString("name");
                    teacher1_info.put(teacher.name, teacher);
                    guidteacherBox1.addItem(teacher.name);
                    
                }
                
            }catch(Exception e) {
                JOptionPane.showMessageDialog(null, "could'nt connect to the db");
                System.out.println("Coudl'nt connect to db");
            }
            finally {

                if(con!=null) {
                    try {
                        con.close();
                } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Please Check your connection");
            e.printStackTrace();
        }
    }//GEN-LAST:event_formWindowOpened

    private void guidteacherBox1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_guidteacherBox1MouseClicked
        // TODO add your handling code here:
        guidteacherBox2.setEnabled(false);
        if (guidteacherBox1.getSelectedItem()!=null) {
            
            try{
                    Connection con=null;
                    try {
                        String db_name="root";
                        String db_pass="";
                        String url="jdbc:mysql://localhost/maindb";
                        Class.forName("com.mysql.jdbc.Driver").newInstance();
                        con=DriverManager.getConnection(url,db_name,db_pass);
                        Statement stmt2=con.createStatement();
                        ResultSet res2=null;
                        t1 = (Teacher)guidteacherBox1.getSelectedItem();
                        res2=stmt2.executeQuery("select * from teachers where name = '"+ t1.name+"'");
                        teachers_id = res2.getString("_id");
                        stmt2.executeUpdate("update students set guidteachers_id ='"+teachers_id+"' where email = '"+studentPage.student.getEmail()+"'");
                        
                    }catch(Exception e) {
                        JOptionPane.showMessageDialog(null, "could'nt connect to the db");
                        System.out.println("Coudl'nt connect to db");
                    }
                    finally {

                        if(con!=null) {
                            try {
                                con.close();
                        } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }

                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "Please Check your connection");
                    e.printStackTrace();
                }
            
            save.setEnabled(true);
            if (!(studentPage.student instanceof BachStudent)) {
                
                guidteacherBox2.setEnabled(true);
                try{
                    Connection con=null;
                    try {
                        String db_name="root";
                        String db_pass="";
                        String url="jdbc:mysql://localhost/maindb";
                        Class.forName("com.mysql.jdbc.Driver").newInstance();
                        con=DriverManager.getConnection(url,db_name,db_pass);
                        Statement stmt2=con.createStatement();
                        ResultSet res2=null;
                        t1 = (Teacher)guidteacherBox1.getSelectedItem();
                        res2=stmt2.executeQuery("select * from teachers where not name = '"+ t1.name+"'");

                        while (res2.next()) {                    
                            Teacher teacher = new Teacher();
                            teacher.name = res2.getString("name");
                            teacher2_info.put(teacher.name, teacher);
                            guidteacherBox2.addItem(teacher.name);

                        }

                    }catch(Exception e) {
                        JOptionPane.showMessageDialog(null, "could'nt connect to the db");
                        System.out.println("Coudl'nt connect to db");
                    }
                    finally {

                        if(con!=null) {
                            try {
                                con.close();
                        } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }

                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "Please Check your connection");
                    e.printStackTrace();
                }


            }
        }
        
        
    }//GEN-LAST:event_guidteacherBox1MouseClicked

    private void guidteacherBox2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_guidteacherBox2MouseClicked
        // TODO add your handling code here:
        
            try{
                    Connection con=null;
                    try {
                        String db_name="root";
                        String db_pass="";
                        String url="jdbc:mysql://localhost/maindb";
                        Class.forName("com.mysql.jdbc.Driver").newInstance();
                        con=DriverManager.getConnection(url,db_name,db_pass);
                        Statement stmt2=con.createStatement();
                        ResultSet res2=null;
                        t2 = (Teacher)guidteacherBox2.getSelectedItem();
                        res2=stmt2.executeQuery("select * from teachers where name = '"+ t2.name+"'");
                        teachers_id += res2.getString("_id");
                        stmt2.executeUpdate("update students set guidteachers_id ='"+teachers_id.substring(0,2)+"' where email = '"+studentPage.student.getEmail()+"'");
                        
                    }catch(Exception e) {
                        JOptionPane.showMessageDialog(null, "could'nt connect to the db");
                        System.out.println("Coudl'nt connect to db");
                    }
                    finally {

                        if(con!=null) {
                            try {
                                con.close();
                        } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                        }
                    }

                }catch(Exception e){
                    JOptionPane.showMessageDialog(null, "Please Check your connection");
                    e.printStackTrace();
                }
    }//GEN-LAST:event_guidteacherBox2MouseClicked

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(specifyTeacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(specifyTeacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(specifyTeacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(specifyTeacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new specifyTeacherPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JComboBox<String> guidteacherBox1;
    private javax.swing.JComboBox<String> guidteacherBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JButton save;
    private javax.swing.JLabel student_name;
    // End of variables declaration//GEN-END:variables
}
