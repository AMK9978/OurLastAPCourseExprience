package mainapp;

public class DocRequest extends Request {

    public Teacher[] outJudgeTeacher;
    public Teacher observerTeacher;

    public DocRequest(Student student, String summary, String guidteachers_id) {
        super(student, summary, guidteachers_id);
    }

    
    public Teacher getObserverTeacher() {
        return observerTeacher;
    }

    public void setObserverTeacher(Teacher observerTeacher) {
        //It should be checked that is not the same as other Request.Teacher
        this.observerTeacher = observerTeacher;
    }

    public Teacher[] getOutJudgeTeacher() {
        return outJudgeTeacher;
    }

    public void setOutJudgeTeacher(Teacher[] outJudgeTeacher) {
        //It should be checked that is not the same as other Request.Teacher
        this.outJudgeTeacher = outJudgeTeacher;
    }

    public Teacher getInJudgeTeacher() {
        return injudgeTeacher;
    }

    public void setJudgeTeacher(Teacher judgeTeacher) {
        //It should be checked that is not the same as other Request.Teacher
        //check if judgeTeacher was in the Student GP true.
        try {
            if (judgeTeacher.GP.equals(this.getStudent().getgp())){
                this.injudgeTeacher = judgeTeacher;
            }
        }catch (Exception e){

        }
    }

    @Override
    public String getSheet() {
        return null;
    }
}
