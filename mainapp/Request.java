package mainapp;

public abstract class Request {

    protected String state = null;//It was better that state be enum,
    /*
    Three state, 1-Accepted , 2-Canceled , 3-Waiting , 4-null
    * */
    private Student student;
    private Dateplace dateplace;
    private String summery;
    protected Teacher injudgeTeacher;
    protected String guidteachers_id;
    
    public Request(Student student, String summary, String guidteachers_id){
        setSummery(summery);
        setStudent(student);
        setState("waiting");
        setGuidteachers_id(guidteachers_id);
    }
    
    public abstract Teacher [] getGuidTeachers();
    
    public String getGuidteachers_id() {
        return guidteachers_id;
    }

    public void setGuidteachers_id(String guidteachers_id) {
        this.guidteachers_id = guidteachers_id;
    }
    
     public Dateplace getdateplace() {
        return dateplace;
    }

    public void setdateplace(Dateplace dateplace) {
        this.dateplace = dateplace;
    }
    
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public String getDateplace() {
        return String.valueOf(dateplace.getDate())+dateplace.getPlace();
    }

    public String getSummery() {
        return summery;
    }

    public void setSummery(String summery) {
        this.summery = summery;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public abstract String getSheet();
}
