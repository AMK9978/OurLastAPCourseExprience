package mainapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.HashMap;
import javax.swing.JOptionPane;


public class setRequestPage extends javax.swing.JFrame {

    static HashMap<String,Dateplace> dateplaces = new HashMap<>();
    
    public setRequestPage() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        student_name = new javax.swing.JLabel();
        back = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txt_title = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txt_summary = new javax.swing.JTextArea();
        jLabel4 = new javax.swing.JLabel();
        dateplacelist = new javax.swing.JComboBox<>();
        setReq = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("سامانه دفاعیه دانشگاهی");

        student_name.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        student_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        student_name.setText("امبرمحمد کریمی");

        back.setText("بازگشت");
        back.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                backMouseClicked(evt);
            }
        });

        jLabel2.setText("عنوان:");
        jLabel2.setToolTipText("");

        txt_title.setToolTipText("عنوان");

        jLabel3.setText("چکیده / ملاحظات :");

        txt_summary.setColumns(20);
        txt_summary.setRows(5);
        jScrollPane1.setViewportView(txt_summary);

        jLabel4.setText("تعیین مکان و زمان:");

        dateplacelist.setEnabled(false);

        setReq.setText("ثبت");
        setReq.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                setReqMouseClicked(evt);
            }
        });
        setReq.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setReqActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(back)
                        .addGap(363, 363, 363)
                        .addComponent(student_name, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(setReq, javax.swing.GroupLayout.PREFERRED_SIZE, 75, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(layout.createSequentialGroup()
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(txt_title)
                                    .addComponent(jScrollPane1)
                                    .addComponent(dateplacelist, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(37, 37, 37)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel2)
                                    .addComponent(jLabel3)
                                    .addComponent(jLabel4))))
                        .addGap(94, 94, 94))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(back)
                    .addComponent(student_name))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(txt_title, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(50, 50, 50)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel3)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(67, 67, 67)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4)
                    .addComponent(dateplacelist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(44, 44, 44)
                .addComponent(setReq)
                .addGap(112, 112, 112))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void backMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_backMouseClicked
        // TODO add your handling code here:
        this.dispose();
        new studentPage().setVisible(true);
    }//GEN-LAST:event_backMouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        student_name.setText(studentPage.student.name);
        if (studentPage.student.getgp().equals("doc")||studentPage.student.getgp().equals("post")) {
            
            
            
        }
        
        
        if (studentPage.student.getgp().equals("bach")) {
            dateplacelist.setEnabled(true);
            try{
            Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2=stmt2.executeQuery("select * from dateplace where isfree = '"+1+"'");
                
                while (res2.next()) {                    
                    Dateplace dateplace = new Dateplace(res2.getDate("date"),res2.getString("place"),true);
                    dateplaces.put(dateplace.getdateplace(), dateplace);
                    dateplacelist.addItem(dateplace.getdateplace());
                    
                }
                
            }catch(Exception e) {
                JOptionPane.showMessageDialog(null, "could'nt connect to the db");
                System.out.println("Coudl'nt connect to db");
            }
            finally {

                if(con!=null) {
                    try {
                        con.close();
                } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }

        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Please Check your connection");
            e.printStackTrace();
        }
            
        }else{
            dateplacelist.setEnabled(false);
        }
        
        
    }//GEN-LAST:event_formWindowOpened

    private void setReqMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_setReqMouseClicked
        // TODO add your handling code here:
        if (dateplacelist.getSelectedItem()!= null) {
            try{
                Dateplace mydateplace = dateplaces.get((String) dateplacelist.getSelectedItem());
                String title = txt_title.getText();
                String summary = txt_summary.getText();
                try{
                    Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                ResultSet res = stmt2.executeQuery("select * from dateplace where date = '"+mydateplace.getDate()+"' place = '"+mydateplace.getPlace()+"'");
                res2=stmt2.executeQuery("update  dateplace set isfree = '"+0+"' where _id = '"+res.getInt("_id")+"'");
                Request request = new BachRequest(studentPage.student, title+summary, studentPage.student.getguideteachers_id(), mydateplace);
                studentPage.student.setRequest(request);
                
            }catch(Exception e) {
                JOptionPane.showMessageDialog(null, "could'nt connect to the db");
                System.out.println("Coudl'nt connect to db");
            }
            finally {

                if(con!=null) {
                    try {
                        con.close();
                } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }
            }
            
            }catch(Exception e){
                JOptionPane.showMessageDialog(null, "check your input");
                e.printStackTrace();
            }
            
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Please Check your connection");
            e.printStackTrace();
        }
        
        }  
    }//GEN-LAST:event_setReqMouseClicked

    private void setReqActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setReqActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_setReqActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(setRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(setRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(setRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(setRequestPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new setRequestPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton back;
    private javax.swing.JComboBox<String> dateplacelist;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JButton setReq;
    private javax.swing.JLabel student_name;
    private javax.swing.JTextArea txt_summary;
    private javax.swing.JTextField txt_title;
    // End of variables declaration//GEN-END:variables
}
