package mainapp;
import java.util.ArrayList;

public class Teacher {

    public String GP = null;//
    public String name = null;//
    public boolean isManager = false;//
    public ArrayList<String> dateplaceList = new ArrayList<>();
    protected ArrayList<Request> reqGuidList = new ArrayList<>();
    public ArrayList<Request> reqManageList = new ArrayList<>();
    public ArrayList<Request> reqObserveList = new ArrayList<>();
    public ArrayList<Request> reqJudgeList = new ArrayList<>();
    
    public Teacher(String name,String gp,boolean isManager){
        setName(name);
        setGP(gp);
        setManager(isManager);
    }
    
    public String getGP() {
        return GP;
    }

    public void setGP(String GP) {
        this.GP = GP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isManager() {
        return isManager;
    }

    public void setManager(boolean manager) {
        isManager = manager;
    }

    public ArrayList<String> getDateplaceList() {
        return dateplaceList;
    }

    public void setDateplaceList(ArrayList<String> dateplaceList) {
        this.dateplaceList = dateplaceList;
    }

    public ArrayList<Request> getReqGuidList() {
        return reqGuidList;
    }

    public void setReqGuidList(ArrayList<Request> reqGuidList) {
        this.reqGuidList = reqGuidList;
    }

    public ArrayList<Request> getReqManageList() {
        return reqManageList;
    }

    public void setReqManageList(ArrayList<Request> reqManageList) {
        this.reqManageList = reqManageList;
    }

    public ArrayList<Request> getReqObserveList() {
        return reqObserveList;
    }

    public void setReqObserveList(ArrayList<Request> reqObserveList) {
        this.reqObserveList = reqObserveList;
    }

    public ArrayList<Request> getReqJudgeList() {
        return reqJudgeList;
    }

    public void setReqJudgeList(ArrayList<Request> reqJudgeList) {
        this.reqJudgeList = reqJudgeList;
    }
}
