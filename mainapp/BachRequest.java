package mainapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class BachRequest extends Request {

    public BachRequest(Student student, String summary, String guidteachers_id, Dateplace dateplace) {
        super(student, summary, guidteachers_id);
        setdateplace(dateplace);
    }


    public Teacher getInJudgeTeacher() {
        return injudgeTeacher;
    }

    public void setJudgeTeacher(Teacher judgeTeacher) {
        //It should be checked that is not the same as other Request.Teacher
        //check if judgeTeacher was in the Student GP true.
        try {
            if (judgeTeacher.GP.equals(this.getStudent().getgp())){
                this.injudgeTeacher = judgeTeacher;
            }
        }catch (Exception e){

        }
    }

    @Override
    public String getSheet() {
        return String.format("%s %s %s %s %s %s",
                getStudent().name,getSummery(),getGuidTeachers()[0].getName(),
                getInJudgeTeacher(),getDateplace());
    }

    @Override
    public Teacher[] getGuidTeachers() {
        int guid_teacher_id = Integer.parseInt(getGuidteachers_id());
        
        Connection con=null;
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            ResultSet res = stmt2.executeQuery("select * from teachers where _id = '"+guid_teacher_id+"'");
            boolean isManager = true ? res.getInt("ismanager") == 1 : false;
            Teacher teacher = new Teacher(res.getString("name"),res.getString("gp"),isManager);
            Teacher[] teacher_arr = {teacher};
            return teacher_arr;
            
        }catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if(con!=null) {
                try {
                    con.close();
                } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
                }
            }
        }
        return null;
    }
    
}
