package mainapp;

import java.sql.Date;

public class Dateplace {
    private Date date;
    private String place;
    private boolean isfree ;
    private int _id;
    
    public int getId() {
        return _id;
    }
    
    public String getdateplace(){
        return String.format("%s,%s", this.getDate(),this.getPlace());
    }
    public Dateplace(Date date,String place,boolean isfree){
        setDate(date);
        setPlace(place);
        setIsfree(isfree);
    }
    
    public Date getDate() {
        return date;
    }

    public final void setDate(Date date) {
        this.date = date;
    }

    public String getPlace() {
        return place;
    }

    public final void setPlace(String place) {
        this.place = place;
    }

    public boolean isIsfree() {
        return isfree;
    }

    public final void setIsfree(boolean isfree) {
        this.isfree = isfree;
    }
    
}
