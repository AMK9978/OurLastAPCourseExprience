package mainapp;


public class Student {

    protected String name;
    protected int id;
    protected String graduate;
    private Request request;
    private String password,email;
    private String gp;
    private String guidteachers_id;
//    public Student(String name, int ID, String GP) {
//        this.name = name;
//        this.id = ID;
//        this.GP = GP;
//    }
    public Request getRequest() {
        return request;
    }

    public void setRequest(Request request) {
        this.request = request;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getguideteachers_id() {
        return guidteachers_id;
    }

    public void setguidteachers_id(String n) {
        
        this.guidteachers_id = n;
    }
    
    public String getgp() {
        return gp;
    }

    public void setgp(String gp) {
        this.gp = gp;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

    public int getid() {
        return id;
    }

    public void setid(int ID) {
        this.id = ID;
    }

    public String getGraduate() {
        return graduate;
    }

    public void setGraduate(String g) {
        this.graduate = g;
    }
    
}
