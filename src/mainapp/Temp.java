package mainapp;

class Temp{
    private String name;
    private int _id;
    Temp(String n , int _id){
        setName(n);
        set_Id(_id);
    }
    
    public String getName(){
        return this.name;
    }
    
    public void setName(String n){
        this.name = n;
    
    }
    
    public int get_Id(){
        return this._id;
    }
    
    public void set_Id(int i){
        this._id = i;
    }
}
