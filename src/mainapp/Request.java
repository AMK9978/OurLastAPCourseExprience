package mainapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

public abstract class Request {
    
    /*
    Three state, 1-Accepted , 2-Canceled , 3-Waiting , 4-null
    * */
    public int _id;
    private Student student;
    private Dateplace dateplace;
    private String summery;
    protected String guidteachers_id;
    protected Teacher injudgeTeacher;
    protected int injudgeteacher_id;
    protected String outjudgeteachers_id;
    protected Teacher[] outjudgeteachers;
    protected Teacher observer;
    protected int observerteacher_id;
    protected String state = null;
    protected String graduate;
    
    public Request(Student student, String summary, String guidteachers_id){
        setSummery(summary);
        setStudent(student);
        setState("waiting");
        setGuidteachers_id(guidteachers_id);
    }
    
    
     public Teacher getInjudgeTeacher() {
        return injudgeTeacher;
    }

    public void setInjudgeTeacher(Teacher injudgeTeacher) {
        this.injudgeTeacher = injudgeTeacher;
    }

    public int getInjudgeteacher_id() {
        return injudgeteacher_id;
    }

    public void setInjudgeteacher_id(int injudgeteacher_id) {
        this.injudgeteacher_id = injudgeteacher_id;
    }

    public String getOutjudgeteachers_id() {
        return outjudgeteachers_id;
    }

    public void setOutjudgeteachers_id(String outjudgeteachers_id) {
        this.outjudgeteachers_id = outjudgeteachers_id;
    }

    public Teacher[] getOutjudgeteachers() {
        return outjudgeteachers;
    }

    public void setOutjudgeteachers(Teacher[] outjudgeteachers) {
        this.outjudgeteachers = outjudgeteachers;
    }

    public Teacher getObserver() {
        return observer;
    }

    public void setObserver(Teacher observer) {
        this.observer = observer;
    }

    public int getObserverteacher_id() {
        return observerteacher_id;
    }

    public void setObserverteacher_id(int observerteacher_id) {
        this.observerteacher_id = observerteacher_id;
    }
    
    public int get_id(){
        return this._id;
    }
    
    public void set_id(Student student){
        Connection con=null;
        int id = 0;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                System.out.println("Man dar Request set_id()");
                System.out.println(student.get_id());
                res2 = stmt2.executeQuery("select * from requests where student_id='"+student.get_id()+"'");
                if (res2.next()) {
                    id = res2.getInt("_id");
                    System.out.println("Dar set_id:"+id);
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        this._id = id;
    }
    
    public String getGuidteachers_id() {
        return guidteachers_id;
    }

    public void setGuidteachers_id(String guidteachers_id) {
        this.guidteachers_id = guidteachers_id;
    }
    
     public Dateplace getdateplace() {
        return dateplace;
    }

    public void setdateplace(Dateplace dateplace) {
        this.dateplace = dateplace;
    }
    
    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
        
        
    }

    public String getDateplace() {
        return String.valueOf(dateplace.getDate())+dateplace.getPlace();
    }

    public String getSummery() {
        return summery;
    }

    public void setSummery(String summery) {
        this.summery = summery;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
        Connection con=null; 
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            stmt2.executeQuery("update requests set state ='"+state+"' where _id='"+this._id+"'");
        }
        catch (Exception e){
            e.printStackTrace();
        } 
        
    }

    public abstract String getSheet();
}
