package mainapp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

public class Expert {
    
    static ResultSet resultSet;
    
    public static String find_and_input(ArrayList<Integer> arr,int _id){
        String answer = "";
        try {
            ArrayList <Integer> arrayList = new ArrayList<>();
            arrayList = arr;
            arrayList.add(_id);

            for (int i = 0; i < arrayList.size(); i++) {
                answer +=Integer.toString(arrayList.get(i))+",";
            }
        } catch (Exception e) {
        }
        return answer;
        
    }
    
    public static void updateObserve(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverlist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingobserverlist_id='"+str+"' where _id='"+id+"'");
                }                
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    /*public static void updateDateplace(int request_id,int dateplace_id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from dateplace where _id='"+dateplace_id+"'");
                if (res2.next()) {
                    stmt2.executeUpdate("update dateplace set isfree ='0' where _id='"+dateplace_id+"'");
                }                
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }*/
    
    public static int updatedateplace(int _id){
        int dateplace_id = 0;
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from dateplace where _id ='"+_id+"'");
                if (res2.next()) {
                    dateplace_id = res2.getInt("_id");
                    System.out.println("Dar Hale Tagheer dadan dateplace , updatedateplace");
                    stmt2.executeUpdate("update dateplace set isfree='"+0+"' where  _id='"+dateplace_id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        return dateplace_id;
    }
    
    public static void setForChoosenObserver(Request request){
        
    }
    
    public static ArrayList<Teacher> getForObserver(int request_id){
        //get all teachers except request's guiders and judges
        Request request = find_Request(request_id);
        Connection con=null;
        ArrayList<Teacher> arrayList = new ArrayList<>();
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers");
                while (res2.next()) {
                    try{
                        if (((int)request.getGuidteachers_id().charAt(0)==res2.getInt("_id"))||(res2.getInt("_id"))==(int)request.getGuidteachers_id().charAt(0))
                            continue;
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                    arrayList.add(new Teacher(res2.getString("name"),res2.getString("gp"), true?res2.getInt("ismanager")==1:false));
                }
            }catch(Exception e){
                e.printStackTrace();
            }                    

        return arrayList;
    }
    
    public static ArrayList<Dateplace> getDateplaces(){
        //get all available dateplaces that their check is true
        ArrayList<Dateplace> arrayList = new ArrayList<>();
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from dateplace where isfree ='"+1+"'");
                while (res2.next()) {
                    arrayList.add(new Dateplace(res2.getDate("date"), res2.getString("place"), true, res2.getInt("_id")));
                }
            }catch(Exception e){
                e.printStackTrace();
            }               
        return arrayList;
    }
    
    public static ArrayList<Integer> parseString(String str){
        ArrayList<Integer> arrayList = new ArrayList<>();
        try{
            String s[] = str.split(",");
            for (int i = 0; i < s.length; i++) {
                arrayList.add(Integer.parseInt(s[i]));
            }
        }catch(Exception e){}
        return arrayList;
    }
    
    public static ArrayList<Temp> getObserverRequests(){
        Connection con=null;
        ArrayList<Temp> arrayList = new ArrayList<>();
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2 = stmt2.executeQuery("select * from experttable");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverfind_id");
                    arrayList = getObserveArray(parseString(str));
                }
                
            }catch(Exception e){
                e.printStackTrace();
            }                    
            
        return arrayList;
    }
    
    private static ArrayList<Temp> getObserveArray(ArrayList<Integer> arr) {
        ArrayList<Temp>arrayList = new ArrayList<>();
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                for (int i = 0; i < arr.size(); i++) {
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from requests where _id='"+arr.get(i)+"'");
                if (res2.next()) {
                    Temp temp = new Temp(res2.getString("summary"), res2.getInt("_id"));
                    arrayList.add(temp);
                }
            }
        }
        catch(Exception e){
                e.printStackTrace();
            }
        
        return arrayList;
    }
    
    public static ArrayList<Temp> getDateplaceRequests(){
        Connection con=null;
        ArrayList<Temp>arrayList = new ArrayList<>();
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from experttable");
                if (res2.next()) {
                    String str = res2.getString("waitingdateplace_id");
                    arrayList = getDateplaceArray(parseString(str));
                }
            }catch(Exception e){
                e.printStackTrace();
            }                    

        return arrayList;
    }
    
    public static void updateWaitingObserveList(int request_id,int observer_id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+observer_id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverlist_id");
                    str = find_and_input(parseString(str), request_id);
                    stmt2.executeUpdate("update teachers set waitingobserverlist_id='"+str+"' where _id='"+observer_id+"'");
                }                
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public static void InsertIntoReq(int dateplace_id,int request_id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                stmt2.executeUpdate("update requests set dateplace_id='"+dateplace_id+"' where _id='"+request_id+"'");
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public static void updateWaitingGuidList(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                Statement stmt3=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from requests where _id='"+request.get_id()+"'");
                if (res2.next()) {
                    ArrayList<Integer> arr = parseString(res2.getString("guidteachers_id"));
                    for (int i = 0; i < arr.size(); i++) {
                        int guidteacher_id = arr.get(i);
                        ResultSet res3 = stmt3.executeQuery("select * from teachers where _id='"+guidteacher_id+"'");
                        if (res3.next()) {
                            System.out.println("in depth!");
                            
                            String str = res3.getString("waitingguidlist_id");
                            str = find_and_input(parseString(str), request.get_id());
                            stmt3.executeUpdate("update teachers set waitingguidlist_id='"+str+"' where _id='"+guidteacher_id+"'");
                        }
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    
    
    public Student find_Student(int _id){
        Student student = null;
        
        
        return student;
    }
    
    public static Request find_Request(int _id,int dateplace_id){
        Request request = null;
        Student student;
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from requests where _id='"+_id+"'");
                if (res2.next()) {
                    if (res2.getString("graduate").equals("post")) {
                        int id = res2.getInt("student_id");
                        student = new PostStudent();
                        Statement stmt3=con.createStatement();
                        stmt3.executeUpdate("update students set dateplace_id ='"+dateplace_id+"' where _id='"+id+"'");
                        ResultSet resultSet = stmt3.executeQuery("select * from students where _id='"+id+"'");
                        if (resultSet.next()) {
                            student.setDateplace_id(resultSet.getInt("dateplace_id"));
                            student.setEmail(resultSet.getString("email"));
                            student.setGraduate(resultSet.getString("graduate"));
                            student.setName(resultSet.getString("name"));
                            student.setPassword(resultSet.getString("password"));
                            student.setRequest_id(resultSet.getInt("request_id"));
                            student.setStudent_id(resultSet.getInt("id"));
                            student.set_id(resultSet.getInt("_id"));
                            student.setgp(resultSet.getString("gp"));
                            student.setguidteachers_id(resultSet.getString("guidteachers_id"));
                        }
                        
                        request = new postRequest(student, res2.getString("summary"),student.getguideteachers_id());
                        
                    }else {
                        int id = res2.getInt("student_id");
                        student = new DocStudent();
                        Statement stmt3=con.createStatement();
                        ResultSet resultSet = stmt3.executeQuery("select * from students where _id='"+id+"'");
                        if (resultSet.next()) {
                            student.setDateplace_id(resultSet.getInt("dateplace_id"));
                            student.setEmail(resultSet.getString("email"));
                            student.setGraduate(resultSet.getString("graduate"));
                            student.setName(resultSet.getString("name"));
                            student.setPassword(resultSet.getString("password"));
                            student.setRequest_id(resultSet.getInt("request_id"));
                            student.setStudent_id(resultSet.getInt("id"));
                            student.set_id(resultSet.getInt("_id"));
                            student.setgp(resultSet.getString("gp"));
                            student.setguidteachers_id(resultSet.getString("guidteachers_id"));
                        }
                        request = new DocRequest(student, res2.getString("summary"),student.getguideteachers_id());
//                        ArrayList<Integer> arrayList = new ArrayList<>();
//                        arrayList = Expert.parseString(student.getguideteachers_id());
//                        for (int i = 0; i < arrayList.size(); i++) {
//                            InsertMyGuidList(request, arrayList.get(i));
//                        }
                    }
                }
        }
        catch(Exception e){
                e.printStackTrace();
            }
        return request;
                            
    }
    
    public static Request find_Request(int _id){
        Request request = null;
        Student student;
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from requests where _id='"+_id+"'");
                if (res2.next()) {
                    if (res2.getString("graduate").equals("post")) {
                        int id = res2.getInt("student_id");
                        student = new PostStudent();
                        Statement stmt3=con.createStatement();
                        ResultSet resultSet = stmt3.executeQuery("select * from students where _id='"+id+"'");
                        if (resultSet.next()) {
                            student.setDateplace_id(resultSet.getInt("dateplace_id"));
                            student.setEmail(resultSet.getString("email"));
                            student.setGraduate(resultSet.getString("graduate"));
                            student.setName(resultSet.getString("name"));
                            student.setPassword(resultSet.getString("password"));
                            student.setRequest_id(resultSet.getInt("request_id"));
                            student.setStudent_id(resultSet.getInt("id"));
                            student.set_id(resultSet.getInt("_id"));
                            student.setgp(resultSet.getString("gp"));
                            student.setguidteachers_id(resultSet.getString("guidteachers_id"));
                        }
                        
                        request = new postRequest(student, res2.getString("summary"),student.getguideteachers_id());
                        
                    }else {
                        int id = res2.getInt("student_id");
                        student = new DocStudent();
                        Statement stmt3=con.createStatement();
                        ResultSet resultSet = stmt3.executeQuery("select * from students where _id='"+id+"'");
                        if (resultSet.next()) {
                            student.setDateplace_id(resultSet.getInt("dateplace_id"));
                            student.setEmail(resultSet.getString("email"));
                            student.setGraduate(resultSet.getString("graduate"));
                            student.setName(resultSet.getString("name"));
                            student.setPassword(resultSet.getString("password"));
                            student.setRequest_id(resultSet.getInt("request_id"));
                            student.setStudent_id(resultSet.getInt("id"));
                            student.set_id(resultSet.getInt("_id"));
                            student.setgp(resultSet.getString("gp"));
                            student.setguidteachers_id(resultSet.getString("guidteachers_id"));
                        }
                        request = new DocRequest(student, res2.getString("summary"),student.getguideteachers_id());
//                        ArrayList<Integer> arrayList = new ArrayList<>();
//                        arrayList = Expert.parseString(student.getguideteachers_id());
//                        for (int i = 0; i < arrayList.size(); i++) {
//                            InsertMyGuidList(request, arrayList.get(i));
//                        }
                    }
                }
        }
        catch(Exception e){
                e.printStackTrace();
            }
        return request;
                            
    }
    
     public static void updateWaitingObserveList(int request_id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from experttable");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverfind_id");
                    str = find_and_delete(parseString(str), request_id);
                    stmt2.executeUpdate("update experttable set waitingobserverfind_id = '"+str+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public static void updatedateplace_Expert(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from experttable");
                if (res2.next()) {
                    String str = res2.getString("waitingdateplace_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update experttable set waitingdateplace_id = '"+str+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public static String find_and_delete(ArrayList<Integer> arr,int _id){
        String answer = "";
        try {
            ArrayList <Integer> arrayList = new ArrayList<>();
            arrayList = arr;
            arrayList.remove(_id);

            for (int i = 0; i < arrayList.size(); i++) {
                answer +=Integer.toString(arrayList.get(i))+",";
            }
        } catch (Exception e) {
        }
        return answer;
    }
    
    
    public static void InsertMyGuidList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingguidlist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingguidlist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    private static ArrayList<Temp> getDateplaceArray(ArrayList<Integer> arr) {
        ArrayList<Temp>arrayList = new ArrayList<>();
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                for (int i = 0; i < arr.size(); i++) {
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from requests where _id='"+arr.get(i)+"'");
                if (res2.next()) {
                    Temp temp = new Temp(res2.getString("summary"), res2.getInt("_id"));
                    arrayList.add(temp);
                }
            }
        }
        catch(Exception e){
                e.printStackTrace();
            }
        
        return arrayList;
    }
    
//    
//    public static boolean check(Request request,Date date){
//        Connection con=null;
//        ArrayList<Integer>num = new ArrayList<>();
//        try {
//            String db_name="root";
//            String db_pass="";
//            String url="jdbc:mysql://localhost/maindb";
//            Class.forName("com.mysql.jdbc.Driver").newInstance();
//            con=DriverManager.getConnection(url,db_name,db_pass);
//            Statement stmt2=con.createStatement();
//            ArrayList<Integer> arr = parseString(request.getGuidteachers_id());
//            ResultSet resultSet = stmt2.executeQuery("select * from teachers where _id = '"+arr.get(0)+"'");
//            if (resultSet.next()) {
//                String teacher_dateplace_id = resultSet.getString("dateplacelist_id");
//                try{
//                    num = parseString(teacher_dateplace_id);
//                    for (int i = 0; i < num.size(); i++) {
//                     ResultSet resultSet1 = stmt2.executeQuery("select * dateplace where _id = '"+num.get(0)+"'");
//                    if (resultSet1.next()) {
//                        Date mydate = resultSet1.getDate("date");
//                        if (mydate == request.getdateplace().getDate()) {
//                            return false;
//                        }
//                        
//                    }  
//                    }
//                    
//                }catch(Exception e){e.printStackTrace();}
//                    
//                }catch(Exception e){}
//                try{
//                resultSet = stmt2.executeQuery("select * from teachers where _id = '"+guid_teachers_id[1]+"'");
//                if (resultSet.next()) {
//                teacher_dateplace_id = resultSet.getString("dateplacelist_id");
//                try{
//                    String[] s = teacher_dateplace_id.split(",");
//                    for (int i = 0; i < s.length; i++) {
//                    int num = Integer.parseInt(s[i]);
//                    resultSet = stmt2.executeQuery("select * dateplace where _id = '"+num+"'");
//                    if (resultSet.next()) {
//                        Date date = resultSet.getDate("date");
//                        if (date == request.getdateplace().getDate()) {
//                            return false;
//                        }
//                        
//                    }
//                }//end for means that there was no overlap between student dateplace and teacher's dates
//                    
//                }catch(Exception e){}
//                }
//                }catch(Exception e2){}
//                return true;
//            
//        }catch(Exception e){
//            e.printStackTrace();
//            JOptionPane.showMessageDialog(null, "Check your connection");
//        }
//        return false;
//    }

    

}
