package mainapp;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;

public class expertPage extends javax.swing.JFrame {

    HashMap<String, Integer> requests = new HashMap<>();
    HashMap<String, Integer> dateplaces = new HashMap<>();
    HashMap<String, Teacher> teacher_for_observe = new HashMap<>();
    public expertPage() {
        initComponents();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        exit = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        student_info = new javax.swing.JLabel();
        title_guidteachername = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        waitingobserverlist = new javax.swing.JComboBox<>();
        observerlist = new javax.swing.JComboBox<>();
        jLabel5 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        set_observer = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        dateplace_list = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        set_dateplace = new javax.swing.JButton();
        waitingfordateplacelist = new javax.swing.JComboBox<>();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        exit.setText("خروج");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("سامانه دفاعیه دانشگاهی");

        student_info.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        student_info.setText("امبرمحمد کریمی دانشجوی کارشناسی نرم افزار");

        title_guidteachername.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        title_guidteachername.setText("عنوان     +نام اساتید راهنما");

        waitingobserverlist.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                waitingobserverlistItemStateChanged(evt);
            }
        });
        waitingobserverlist.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                waitingobserverlistActionPerformed(evt);
            }
        });

        jLabel5.setText("تعیین ناظر:");

        jLabel8.setText("لیست درخواست های");

        jLabel7.setText("درحال انتظار:");

        set_observer.setText("تایید");
        set_observer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                set_observerMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(397, 397, 397)
                .addComponent(jLabel7)
                .addGap(52, 52, 52))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(28, 28, 28)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(set_observer, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(407, 407, 407))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(observerlist, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(waitingobserverlist, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(72, 72, 72)
                                .addComponent(jLabel5)
                                .addGap(81, 81, 81))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel8)
                                .addGap(23, 23, 23))))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(48, 48, 48)
                        .addComponent(waitingobserverlist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(jLabel8)))
                .addGap(18, 18, 18)
                .addComponent(jLabel7)
                .addGap(37, 37, 37)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(observerlist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(46, 46, 46)
                .addComponent(set_observer)
                .addGap(45, 45, 45))
        );

        jTabbedPane1.addTab("درخواست های ناظر", jPanel2);

        dateplace_list.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                dateplace_listItemStateChanged(evt);
            }
        });

        jLabel6.setText("تعیین زمان و مکان:");

        set_dateplace.setText("تایید");
        set_dateplace.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                set_dateplaceMouseClicked(evt);
            }
        });

        waitingfordateplacelist.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                waitingfordateplacelistItemStateChanged(evt);
            }
        });

        jLabel3.setText("لیست درخواست های");

        jLabel4.setText("درحال انتظار:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(46, 46, 46)
                .addComponent(set_dateplace, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(389, 389, 389))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(waitingfordateplacelist, javax.swing.GroupLayout.Alignment.LEADING, 0, 280, Short.MAX_VALUE)
                    .addComponent(dateplace_list, javax.swing.GroupLayout.Alignment.LEADING, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(74, 74, 74)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(39, 39, 39))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(47, 47, 47)
                        .addComponent(waitingfordateplacelist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)))
                .addGap(41, 41, 41)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dateplace_list, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addGap(60, 60, 60)
                .addComponent(set_dateplace)
                .addGap(47, 47, 47))
        );

        jTabbedPane1.addTab("درخواست های زمان و مکان", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(64, 64, 64)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 540, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(145, 145, 145))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(exit)
                .addGap(232, 232, 232)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(student_info, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(33, 33, 33))
                    .addComponent(title_guidteachername, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(exit)
                    .addComponent(student_info))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(title_guidteachername)
                .addGap(84, 84, 84)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 329, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(109, 109, 109))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        // TODO add your handling code here:
        System.exit(1);
    }//GEN-LAST:event_exitMouseClicked

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_exitActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        waitingfordateplacelist.setSelectedItem(null);
        waitingobserverlist.setSelectedItem(null);
        for (Temp temp : Expert.getObserverRequests()) {
            requests.put(temp.getName(), temp.get_Id());
            waitingobserverlist.addItem(temp.getName());
        }
        for (Temp temp : Expert.getDateplaceRequests()) {
            requests.put(temp.getName(), temp.get_Id());
            waitingfordateplacelist.addItem(temp.getName());
        }
    }//GEN-LAST:event_formWindowOpened

    private void set_dateplaceMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_set_dateplaceMouseClicked
        // TODO add your handling code here:
        //put the dateplace'id in request's dateplace_id >> send it for his guidteacher(s)
        if (dateplace_list.getSelectedItem()!=null&&waitingfordateplacelist.getSelectedItem()!=null) {
            String str = (String) dateplace_list.getSelectedItem();
            String s[] = str.split(",");
            Date date = Date.valueOf(s[0]);
            String place = s[1];
            System.out.println(place+" "+date);
            //check all the dateplaces with this properties to find selected dateplace and set it's isfree 0 and
            //return it's id
            //to change dateplace state. isfree from 1 changes to 0:
            int dateplace_id = Expert.updatedateplace(dateplaces.get((String) dateplace_list.getSelectedItem()));
            //to delete it from dateplace req's of expert:
            Expert.updatedateplace_Expert(Expert.find_Request(requests.get((String)waitingfordateplacelist.getSelectedItem()),dateplace_id));
            //
            Expert.updateWaitingGuidList(Expert.find_Request(requests.get((String)waitingfordateplacelist.getSelectedItem()),dateplace_id));
            Expert.InsertIntoReq( dateplace_id,requests.get((String)waitingfordateplacelist.getSelectedItem()));
            waitingfordateplacelist.removeItem(waitingfordateplacelist.getSelectedItem());
            dateplace_list.removeItem(dateplace_list.getSelectedItem());
        }
        
    }//GEN-LAST:event_set_dateplaceMouseClicked

    private void set_observerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_set_observerMouseClicked
        // TODO add your handling code here:
        if (waitingobserverlist.getSelectedItem()!=null&&observerlist.getSelectedItem()!=null) {
            //put the req'_id in observer's waitingobservelist_id
            int req_id = requests.get((String)waitingobserverlist.getSelectedItem());
            int observer_id = teacher_for_observe.get((String)observerlist.getSelectedItem()).getID();
            Expert.updateWaitingObserveList(req_id, observer_id);
            Expert.updateWaitingObserveList(req_id);
            waitingobserverlist.removeItem(waitingobserverlist.getSelectedItem());
            observerlist.removeItem(observerlist.getSelectedItem());
        }
    }//GEN-LAST:event_set_observerMouseClicked

    private void waitingobserverlistActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_waitingobserverlistActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_waitingobserverlistActionPerformed

    private void waitingfordateplacelistItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_waitingfordateplacelistItemStateChanged
        // TODO add your handling code here:
        if (waitingfordateplacelist.getSelectedItem()!=null) {
            int _id = requests.get((String)waitingfordateplacelist.getSelectedItem());
            for (Dateplace dateplace : Expert.getDateplaces()) {
                dateplace_list.addItem(dateplace.getDate()+","+dateplace.getPlace());
                dateplaces.put(dateplace.getDate()+","+dateplace.getPlace(), dateplace.getId());
            }
        }
    }//GEN-LAST:event_waitingfordateplacelistItemStateChanged

    private void waitingobserverlistItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_waitingobserverlistItemStateChanged
        // TODO add your handling code here:
        if (waitingobserverlist.getSelectedItem()!=null) {
//            Request request = requests.get((String)waitingobserverlist.getSelectedItem());
            int _id = requests.get((String)waitingobserverlist.getSelectedItem());
            for (Teacher teacher : Expert.getForObserver(_id)) {
                try{
                    if (teacher.getID()==_id) {
                        continue;
                    }
                }catch(Exception e){
                    e.printStackTrace();
                }
                observerlist.addItem(teacher.getName());
                teacher_for_observe.put(teacher.name, teacher);
            }
        }
        
    }//GEN-LAST:event_waitingobserverlistItemStateChanged

    private void dateplace_listItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_dateplace_listItemStateChanged
        // TODO add your handling code here:
        
        
    }//GEN-LAST:event_dateplace_listItemStateChanged

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(expertPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(expertPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(expertPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(expertPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new expertPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> dateplace_list;
    private javax.swing.JButton exit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JComboBox<String> observerlist;
    private javax.swing.JButton set_dateplace;
    private javax.swing.JButton set_observer;
    private javax.swing.JLabel student_info;
    private javax.swing.JLabel title_guidteachername;
    private javax.swing.JComboBox<String> waitingfordateplacelist;
    private javax.swing.JComboBox<String> waitingobserverlist;
    // End of variables declaration//GEN-END:variables
}
