package mainapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;


public abstract class Student {

    protected String name;
    private int _id;
    protected String graduate;
    protected Request request;
    private String password,email;
    private String gp;
    private String guidteachers_id;
    private int student_id;
    private int dateplace_id;
    private int request_id;
    
    public static ArrayList<Integer> parseString(String str){
        ArrayList<Integer> arrayList = new ArrayList<>();
        try{
            String s[] = str.split(",");
            for (int i = 0; i < s.length; i++) {
                arrayList.add(Integer.parseInt(s[i]));
            }
        }catch(Exception e){}
        return arrayList;
    }
    public int getDateplace_id() {
        return dateplace_id;
    }

    public void setDateplace_id(int _id) {
        Connection con=null;
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();        
            stmt2.executeUpdate("update students set dateplace_id = '"+_id+"' where id='"+this.getStudent_id()+"'");
            
        }catch(Exception e){
            e.printStackTrace();
        }
        
        
    }
    
    public void setRequest_id(int id){
        this.request_id = id;
    }
    
    public int getRequest_id(){
        return this.request_id;
    }
    
    public int getStudent_id() {
        return student_id;
    }

    public void setStudent_id(int student_id) {
        this.student_id = student_id;
    }
    
    public Request getRequest(){
        return this.request;
    }
    
    public abstract void setRequest(Request request);
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getguideteachers_id() {
        return guidteachers_id;
    }

    public void setguidteachers_id(String n) {
        
        this.guidteachers_id = n;
    }
    
    public String getgp() {
        return gp;
    }

    public void setgp(String gp) {
        this.gp = gp;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String pass) {
        this.password = pass;
    }

    public int get_id() {
        return _id;
    }
    
    public void set_id(int id){
        this._id = id;
    }

    public String getGraduate() {
        return graduate;
    }

    public void setGraduate(String g) {
        this.graduate = g;
    }
    
}
