package mainapp;

import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;

public class BachStudent extends Student {
    
    ResultSet resultSet;
    
    public boolean check(Request request){
        Connection con=null;
        this.request = request;
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            int num =0;
            try{
                String[] s = request.getGuidteachers_id().split(",");
                for (int i = 0; i < s.length; i++) {
                    num = Integer.parseInt(s[i]);
                }
            }catch(Exception e4){
}
            resultSet = stmt2.executeQuery("select * from teachers where _id = '"+num+"'");
            if (resultSet.next()) {
                String teacher_dateplace_id = resultSet.getString("dateplacelist_id");
                try{
                    String[] s = teacher_dateplace_id.split(",");
                    for (int i = 0; i < s.length; i++) {
                        num = Integer.parseInt(s[i]);
                        resultSet = stmt2.executeQuery("select * from dateplace where _id = '"+num+"'");
                        if (resultSet.next()) {
                            Date date = resultSet.getDate("date");
                            if (date == request.getdateplace().getDate()) {
                                return false;
                            }

                        }
                }//end for means that there was no overlap between student dateplace and teacher's dates
                    
                }catch(Exception e){}
                return true;
            }//end if 
        }catch(Exception e){
            e.printStackTrace();
            JOptionPane.showMessageDialog(null, "Check your connection");
        }
        return false;
    }
    
    @Override
    public void setRequest(Request request2) {
        Connection con=null;
        this.request = request2;
        if (!check(request)) {
            return;
        }
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            stmt2.executeUpdate("insert into requests (student_id,guidteachers_id,summary,state,dateplace_id,graduate)"
                + " values('"+request.getStudent().get_id()+"','"+request.getGuidteachers_id()+"','"+request.getSummery()+"','"+request.getState()+"','"+request.getdateplace().getId()+"','"+request.getStudent().graduate+"')");
            ResultSet res = stmt2.executeQuery("select * from requests where student_id = '"+request.getStudent().get_id()+"'");
            if (res.next()) {
                request._id=(res.getInt("_id"));
            }
            
            System.out.println("Hamin aval:"+request.get_id());
            resultSet = stmt2.executeQuery("select * from teachers where _id = '"+parseString(request.getGuidteachers_id()).get(0)+"'");
            
            if (resultSet.next()) {
                String waiting_guid_list_id = resultSet.getString("waitingguidlist_id");
                if (waiting_guid_list_id==null) {
                    waiting_guid_list_id ="";
                }
                waiting_guid_list_id += request.get_id()+",";
                stmt2.executeUpdate("update teachers set waitingguidlist_id = '"+waiting_guid_list_id+"' where _id='"+parseString(this.getguideteachers_id()).get(0)+"'");
            }
            
            stmt2.executeUpdate("update students set dateplace_id ='"+request.getStudent().getDateplace_id()+"' where id='"+request.getStudent().getStudent_id()+"'");
            System.out.println("req_id dar setRequest:"+request.get_id());
            
        }catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if(con!=null) {
                try {
                    con.close();
                } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
                }
            }
        }
    }

}
