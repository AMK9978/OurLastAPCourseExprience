package mainapp;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class postRequest extends Request{

    public Teacher outJudgeTeacher;
    public Teacher observerTeacher;

    public postRequest(Student student, String summary, String guidteachers_id) {
        super(student, summary, guidteachers_id);
        set_id(student);
    }

    public Teacher getObserverTeacher() {
        return observerTeacher;
    }

    public void setObserverTeacher(Teacher observerTeacher) {
        //It should be checked that is not the same as other Request.Teacher
        this.observerTeacher = observerTeacher;
        Connection con=null; 
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            stmt2.executeQuery("update requests set observerteacher_id ='"+observerTeacher.getID()+"'");
            
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public Teacher getOutJudgeTeacher() {
        return outJudgeTeacher;
    }

    public void setOutJudgeTeacher(Teacher outJudgeTeacher) {
        //It should be checked that is not the same as other Request.Teacher
        this.outJudgeTeacher = outJudgeTeacher;
        Connection con=null; 
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            stmt2.executeQuery("update requests set outjudgeteachers_id ='"+Integer.toString(outJudgeTeacher.getID())+","+"'");
            
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public Teacher getInJudgeTeacher() {
        return injudgeTeacher;
    }

    public void setJudgeTeacher(Teacher judgeTeacher) {
        //check if judgeTeacher was in the Student GP true.
        //It should be checked that is not the same as other Request.Teacher
        this.injudgeTeacher = judgeTeacher;
        Connection con=null; 
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            stmt2.executeQuery("update requests set injudgeteacher_id ='"+judgeTeacher.getID()+"'");
            
        }
        catch (Exception e){
            e.printStackTrace();
        }        

    }

    @Override
    public String getSheet() {
        return null;
    }

}
