package mainapp;

import java.util.Date;
import java.util.Properties;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 *
 * @author Launcher
 */
public class sendmail {
    
    private final int TLS_port=587;
    private final String HOST="smtp.gmail.com";
    private final String FROM="amk9978@gmail.com";
    private final String USERNAME="amk9978@gmail.com";
    private final String PASSWORD="amkamkamk99";
    private final boolean AUTHENTICATION=true;
    
    public void send_tls_mail(String to[],String subject,String body,Message.RecipientType type){
        Properties pr=new Properties();
        pr.put("mail.host", HOST);
        pr.put("mail.smtp.port",TLS_port);
        pr.put("mail.smtp.starttls.enable",true);
        Authenticator auth=null;
        if(AUTHENTICATION){
            pr.put("mail.smtp.auth",true);
            auth=new Authenticator() {
                PasswordAuthentication pa=new PasswordAuthentication(USERNAME, PASSWORD);
                
                @Override
                protected PasswordAuthentication getPasswordAuthentication() {
                    return pa;
                }
            };
        }
        
        Session session=Session.getDefaultInstance(pr, auth);
        MimeMessage message=new MimeMessage(session);
        
        try {
            message.setFrom(new InternetAddress(FROM));
            InternetAddress recipients[]=new InternetAddress[to.length];
            for(int i=0;i<to.length;i++){
                recipients[i]=new InternetAddress(to[i]);
            }
            message.setRecipients(type, recipients);
            message.setSubject(subject);
            message.setSentDate(new Date());
            message.setText(body);   
            Transport.send(message);
            
        } catch (Exception ex) {
            System.out.println("Error!"+ex.getMessage());
        }    
    }
}