package mainapp;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


public class teacherPage extends javax.swing.JFrame {

    static Teacher teacher;
    Teacher current_guid_teacher,other_guid_teacher;
    ArrayList<Teacher> choosen_out_judges = new ArrayList<>();
    int other_guid_teacher_id = -1;
    static int current_guid_teacher_id;
    int guid_teachers_count;
    HashMap<Integer,Teacher> inteachers = new HashMap<>();
    HashMap<Integer, Teacher> outteachers = new HashMap<>();
    HashMap<String, Integer> middle1 = new HashMap<>();
    HashMap<String, Integer> middle2 = new HashMap<>();
    HashMap<String, Request> requests = new HashMap<>();
    Teacher in_teacher;
    Teacher out_teacher1;
    Teacher out_teacher2;
    
    public teacherPage() {
        initComponents();
    }
    

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        exit = new javax.swing.JButton();
        tabPane = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel7 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        judgespecify_deny = new javax.swing.JButton();
        judgespecify_accept = new javax.swing.JButton();
        waitingjudgereqslist = new javax.swing.JComboBox<>();
        inteacherslist = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        outteacherslist = new javax.swing.JComboBox<>();
        jLabel11 = new javax.swing.JLabel();
        inteacher_save = new javax.swing.JButton();
        outteacher_save = new javax.swing.JButton();
        out_teacher1_name = new javax.swing.JLabel();
        out_teacher2_name = new javax.swing.JLabel();
        in_teacher_name = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        judgewaitinglist = new javax.swing.JComboBox<>();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        judge_txt_student_name = new javax.swing.JLabel();
        judge_txt_dateplace = new javax.swing.JLabel();
        judge_deny = new javax.swing.JButton();
        judge_accept = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        observewaitinglist = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        observe_txt_student_name = new javax.swing.JLabel();
        observe_txt_dateplace = new javax.swing.JLabel();
        observe_deny = new javax.swing.JButton();
        observe_accept = new javax.swing.JButton();
        jPanel1 = new javax.swing.JPanel();
        guidwaitinglist = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        txt_student_name = new javax.swing.JLabel();
        txt_dateplace = new javax.swing.JLabel();
        deny = new javax.swing.JButton();
        accept = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setText("سامانه دفاعیه دانشگاهی");

        exit.setText("خروج");
        exit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                exitMouseClicked(evt);
            }
        });
        exit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitActionPerformed(evt);
            }
        });

        tabPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);

        jLabel8.setText("لیست درخواست های");

        jLabel9.setText("درحال انتظار تعیین داور");

        judgespecify_deny.setText("رد");

        judgespecify_accept.setText("تایید");
        judgespecify_accept.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                judgespecify_acceptMouseClicked(evt);
            }
        });

        waitingjudgereqslist.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                waitingjudgereqslistItemStateChanged(evt);
            }
        });

        inteacherslist.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { " " }));
        inteacherslist.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                inteacherslistItemStateChanged(evt);
            }
        });

        jLabel10.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        jLabel10.setText("لیست اساتید داخل گروه");

        outteacherslist.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                outteacherslistItemStateChanged(evt);
            }
        });

        jLabel11.setText("لیبست اساتید خارج گروه");

        inteacher_save.setText("تایید");
        inteacher_save.setActionCommand("");
        inteacher_save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                inteacher_saveMouseClicked(evt);
            }
        });

        outteacher_save.setText("تایید");
        outteacher_save.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                outteacher_saveMouseClicked(evt);
            }
        });

        out_teacher1_name.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        out_teacher1_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        out_teacher1_name.setText("حسن ملا");

        out_teacher2_name.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        out_teacher2_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        out_teacher2_name.setText("شکوفه رحیمی");

        in_teacher_name.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        in_teacher_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        in_teacher_name.setText("کارشناس نجف آبادی");

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel7Layout.createSequentialGroup()
                    .addGap(117, 117, 117)
                    .addComponent(out_teacher1_name, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(40, 40, 40)
                    .addComponent(outteacher_save, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(221, 221, 221))
                .addGroup(jPanel7Layout.createSequentialGroup()
                    .addGap(25, 25, 25)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addGroup(jPanel7Layout.createSequentialGroup()
                            .addComponent(judgespecify_accept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(48, 48, 48)
                            .addComponent(judgespecify_deny, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                            .addGroup(jPanel7Layout.createSequentialGroup()
                                .addComponent(outteacherslist, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(36, 36, 36)
                                .addComponent(jLabel11))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(jPanel7Layout.createSequentialGroup()
                                        .addComponent(in_teacher_name)
                                        .addGap(70, 70, 70)
                                        .addComponent(inteacher_save, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addComponent(inteacherslist, javax.swing.GroupLayout.PREFERRED_SIZE, 235, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(40, 40, 40)
                                .addComponent(jLabel10)))
                        .addComponent(out_teacher2_name, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel7Layout.createSequentialGroup()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(26, 26, 26)
                        .addComponent(waitingjudgereqslist, javax.swing.GroupLayout.PREFERRED_SIZE, 241, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(49, 49, 49)
                        .addComponent(jLabel9))
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(317, 317, 317)
                        .addComponent(jLabel8)))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(waitingjudgereqslist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(76, 76, 76)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inteacherslist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(inteacher_save)
                    .addComponent(in_teacher_name))
                .addGap(34, 34, 34)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(outteacherslist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(outteacher_save)
                        .addGap(36, 36, 36)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(judgespecify_deny)
                            .addComponent(judgespecify_accept)))
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(out_teacher1_name)
                        .addComponent(out_teacher2_name)))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 529, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 427, Short.MAX_VALUE)
            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel2Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        tabPane.addTab("تعیین داور", jPanel2);

        jLabel6.setText("لیست درخواست های");

        jLabel7.setText("درحال انتظار داوری");

        judge_txt_student_name.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        judge_txt_student_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        judge_txt_student_name.setText("امیرمحمد کریمی");

        judge_txt_dateplace.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        judge_txt_dateplace.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        judge_txt_dateplace.setText("دانشکده فنی قدیم 1397-04-04");

        judge_deny.setText("رد");
        judge_deny.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                judge_denyMouseClicked(evt);
            }
        });

        judge_accept.setText("تایید");
        judge_accept.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                judge_acceptMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(judge_accept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(judge_deny, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(judgewaitinglist, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(judge_txt_student_name, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(judge_txt_dateplace, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(judgewaitinglist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel7)
                .addGap(78, 78, 78)
                .addComponent(judge_txt_student_name)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(judge_txt_dateplace)
                .addGap(97, 97, 97)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(judge_deny)
                    .addComponent(judge_accept))
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 481, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 427, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel3Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        tabPane.addTab("داوری", jPanel3);

        jLabel4.setText("لیست درخواست های");

        jLabel5.setText("درحال انتظار برای نظارت");

        observe_txt_student_name.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        observe_txt_student_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        observe_txt_student_name.setText("امیرمحمد کریمی");

        observe_txt_dateplace.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        observe_txt_dateplace.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        observe_txt_dateplace.setText("دانشکده فنی قدیم 1397-04-04");

        observe_deny.setText("رد");
        observe_deny.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                observe_denyMouseClicked(evt);
            }
        });

        observe_accept.setText("تایید");
        observe_accept.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                observe_acceptMouseClicked(evt);
            }
        });
        observe_accept.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                observe_acceptActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(observe_txt_student_name, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(observe_txt_dateplace, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel5)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addComponent(observe_accept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(48, 48, 48)
                            .addComponent(observe_deny, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(jPanel5Layout.createSequentialGroup()
                            .addComponent(observewaitinglist, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(18, 18, 18)
                            .addComponent(jLabel4))))
                .addGap(24, 24, 24))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(63, 63, 63)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(observewaitinglist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel5)
                .addGap(78, 78, 78)
                .addComponent(observe_txt_student_name)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(observe_txt_dateplace)
                .addGap(97, 97, 97)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(observe_deny)
                    .addComponent(observe_accept))
                .addGap(34, 34, 34))
        );

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 489, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 427, Short.MAX_VALUE)
            .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel4Layout.createSequentialGroup()
                    .addGap(0, 0, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGap(0, 0, Short.MAX_VALUE)))
        );

        tabPane.addTab("ناظر", jPanel4);

        jLabel2.setText("لیست درخواست های");

        jLabel3.setText("درحال انتظار راهنمایی");

        txt_student_name.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txt_student_name.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txt_student_name.setText("امیرمحمد کریمی");

        txt_dateplace.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txt_dateplace.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        txt_dateplace.setText("دانشکده فنی قدیم 1397-04-04");

        deny.setText("رد");
        deny.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                denyMouseClicked(evt);
            }
        });

        accept.setText("تایید");
        accept.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                acceptMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(accept, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(48, 48, 48)
                        .addComponent(deny, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(guidwaitinglist, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel2))))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(61, 61, 61)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_student_name, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_dateplace, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 405, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(76, 76, 76)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(guidwaitinglist, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addGap(78, 78, 78)
                .addComponent(txt_student_name)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_dateplace)
                .addGap(97, 97, 97)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deny)
                    .addComponent(accept))
                .addGap(47, 47, 47))
        );

        tabPane.addTab("راهنما", jPanel1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 630, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(exit)
                .addGap(548, 548, 548))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(86, 86, 86)
                .addComponent(tabPane, javax.swing.GroupLayout.PREFERRED_SIZE, 486, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(58, 58, 58))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(exit)
                .addGap(9, 9, 9)
                .addComponent(tabPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(31, 31, 31))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_exitMouseClicked
        // TODO add your handling code here:
        saveDetails(current_guid_teacher_id);
        System.exit(1);
    }//GEN-LAST:event_exitMouseClicked

    private void exitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_exitActionPerformed

    
    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        // TODO add your handling code here:
        guidwaitinglist.setSelectedItem(null);
        waitingjudgereqslist.setSelectedItem(null);
        outteacher_save.setEnabled(false);
        inteacher_save.setEnabled(false);
        judgespecify_accept.setEnabled(false);
        judgespecify_deny.setEnabled(false);
        judgewaitinglist.setSelectedItem(null);
        tabPane.setSelectedIndex(3);
        tabPane.setEnabledAt(0, false);
        if (teacherPage.teacher.isManager) {
            tabPane.setEnabledAt(0, true);
        }
        for (Request request : teacherPage.teacher.getReqGuidList()) {
            requests.put(request.getStudent().getName(), request);
            guidwaitinglist.addItem(request.getStudent().getName());
        }
        for (Request request : teacherPage.teacher.getReqJudgeList()) {
            requests.put(request.getStudent().getName(), request);
            judgewaitinglist.addItem(request.getStudent().getName());
        }
        for (Request request : teacherPage.teacher.getReqObserveList()) {
            requests.put(request.getStudent().getName(), request);
            observewaitinglist.addItem(request.getStudent().getName());
        }
        for (Request request : teacherPage.teacher.getReqManageList()) {
            requests.put(request.getStudent().getName(), request);
            waitingjudgereqslist.addItem(request.getStudent().getName());
        }
        
    }//GEN-LAST:event_formWindowOpened

    private void acceptMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_acceptMouseClicked
        // TODO add your handling code here:
        
        if (guidwaitinglist.getSelectedItem()!=null) {
            Request request = requests.get((String)guidwaitinglist.getSelectedItem());
            System.out.println("req_id in pressing:"+request.get_id());
            String s = request.getGuidteachers_id();
            ArrayList<Integer> _ids = new ArrayList<>();
            _ids = parseString(s);
            guid_teachers_count = _ids.size();
            current_guid_teacher_id = teacherPage.teacher.getID();
            if (_ids.get(0) == teacherPage.teacher.getID()&&guid_teachers_count==2) {
                other_guid_teacher_id = _ids.get(1);
            }else if (_ids.get(0) != teacherPage.teacher.getID()&&guid_teachers_count==2) {
                other_guid_teacher_id = _ids.get(0);
            }
            System.out.println(request.get_id());
            System.out.println(current_guid_teacher_id);
            accept_guid_Req(request, true);
            guidwaitinglist.removeItem(guidwaitinglist.getSelectedItem());
        }
    }//GEN-LAST:event_acceptMouseClicked

    private void denyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_denyMouseClicked
        // TODO add your handling code here:
        if (guidwaitinglist.getSelectedItem()!=null) {
            Request request = requests.get((String)guidwaitinglist.getSelectedItem());
            String s = request.getGuidteachers_id();
            ArrayList<Integer> _ids = new ArrayList<>();
            _ids = parseString(s);
            guid_teachers_count = _ids.size();
            current_guid_teacher_id = teacherPage.teacher.getID();
            if (_ids.get(0) == teacherPage.teacher.getID()&&guid_teachers_count==2) {
                other_guid_teacher_id = _ids.get(1);
            }else if (_ids.get(0) != teacherPage.teacher.getID()&&guid_teachers_count==2) {
                other_guid_teacher_id = _ids.get(0);
            }
            accept_guid_Req(request, false);
            guidwaitinglist.removeItem(guidwaitinglist.getSelectedItem());
        }
    }//GEN-LAST:event_denyMouseClicked

    private void observe_acceptMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_observe_acceptMouseClicked
       if (observewaitinglist.getSelectedItem()!=null) {
            Request request = requests.get((String)observewaitinglist.getSelectedItem());
            updateWaitingObserveList(request, current_guid_teacher_id);
            InsertMyObserveList(request, current_guid_teacher_id);
            InsertMyDateplace(request, current_guid_teacher_id);
            Insert_Req_Observer_id(request,current_guid_teacher_id);
            sendmail s = new sendmail();
            request.setState("accepted");
            observewaitinglist.removeItem(observewaitinglist.getSelectedItem());
        } 
    }//GEN-LAST:event_observe_acceptMouseClicked

    private void observe_denyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_observe_denyMouseClicked
        // TODO add your handling code here:
        if (observewaitinglist.getSelectedItem()!=null) {
            Request request = requests.get((String)observewaitinglist.getSelectedItem());
            updateWaitingObserveList(request, current_guid_teacher_id);
            //Insert into expert
            deny_observation(request);
            observewaitinglist.removeItem(observewaitinglist.getSelectedItem());
        }
    }//GEN-LAST:event_observe_denyMouseClicked

    private void judge_acceptMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_judge_acceptMouseClicked
        // TODO add your handling code here:
        if (judgewaitinglist.getSelectedItem()!=null) {
            Request request = requests.get((String)judgewaitinglist.getSelectedItem());
            updateWaitingJudgeList(request, current_guid_teacher_id);
            InsertMyJudgeList(request, current_guid_teacher_id);
            InsertMyDateplace(request, current_guid_teacher_id);
            if (request instanceof BachRequest) {
                request.setState("accepted");
                UpdateState(request);
            }else{
                if (request instanceof postRequest) {
                    if (isInOtherJudgeList(request,current_guid_teacher_id)) {
                        InsertIntoExpert(request);
                    }
                }else{
                    if (isInOtherJudgeList(request, current_guid_teacher_id)) {
                        InsertIntoExpert(request);
                    }
                }
            }
            judgewaitinglist.removeItem(judgewaitinglist.getSelectedItem());
        }
        
        
    }//GEN-LAST:event_judge_acceptMouseClicked

    private void judge_denyMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_judge_denyMouseClicked
        // TODO add your handling code here:
        if (judgewaitinglist.getSelectedItem()!=null) {
            Request request = requests.get((String)judgewaitinglist.getSelectedItem());
            updateWaitingJudgeList(request, current_guid_teacher_id);
            judgewaitinglist.removeItem(guidwaitinglist.getSelectedItem());
            //request injudge or outjudge must be changed
            updateJudge_id(request,current_guid_teacher_id);
            updateManagerList(request);
        }
    }//GEN-LAST:event_judge_denyMouseClicked

    private void waitingjudgereqslistItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_waitingjudgereqslistItemStateChanged
        // TODO add your handling code here:
        //show teachers in combobox
        if (waitingjudgereqslist.getSelectedItem()!=null) {
            inteacher_save.setEnabled(true);
            inteacherslist.setEnabled(true);
            Request request = requests.get((String)waitingjudgereqslist.getSelectedItem());
            getInTeachers(request);
            for (int i = 0; i < inteachers.size(); i++) {
                try{
                    if (((int)request.getGuidteachers_id().charAt(0)==inteachers.get(i).getID())||(inteachers.get(i).getID())==(int)request.getGuidteachers_id().charAt(0))
                        continue;
                }catch(Exception e){
                    e.printStackTrace();
                }
                
                inteacherslist.addItem((String)inteachers.get(i).getName());
            }
            if (request.getStudent().getGraduate().equals("bach")) {
                outteacherslist.setEnabled(false);
                outteacher_save.setEnabled(false);
            }else{
                outteacherslist.setEnabled(true);
                outteacher_save.setEnabled(true);
                getOutTeachers(request);
                for (int i = 0; i < outteachers.size(); i++) {
                    try{
                    if (((int)request.getGuidteachers_id().charAt(0)==outteachers.get(i).getID())||(outteachers.get(i).getID())==(int)request.getGuidteachers_id().charAt(0))
                        continue;
                }catch(Exception e){
                    e.printStackTrace();
                }
                    outteacherslist.addItem(outteachers.get(i).name);
                }
            }
        }
    }//GEN-LAST:event_waitingjudgereqslistItemStateChanged

    private void inteacherslistItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_inteacherslistItemStateChanged
        // TODO add your handling code here
    }//GEN-LAST:event_inteacherslistItemStateChanged

    private void outteacherslistItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_outteacherslistItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_outteacherslistItemStateChanged

    private void inteacher_saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_inteacher_saveMouseClicked
        // TODO add your handling code here:
        //insert inteacher with conditon of check
        if (inteacherslist.getSelectedItem()!=null) {
            Teacher teacher = inteachers.get(middle1.get((String)inteacherslist.getSelectedItem()));
            in_teacher = teacher;
            in_teacher_name.setText(in_teacher.getName());
        }
    }//GEN-LAST:event_inteacher_saveMouseClicked

    private void outteacher_saveMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_outteacher_saveMouseClicked
        // TODO add your handling code here:
        //insert outteacher with conditon of check
        if (outteacherslist.getSelectedItem()!=null) {
            Teacher teacher = outteachers.get(middle2.get((String)outteacherslist.getSelectedItem()));
            Request request = requests.get((String)waitingjudgereqslist.getSelectedItem());
            choosen_out_judges.add(teacher);
            if (request.getStudent().getGraduate().equals("post")) {
                outteacherslist.setEnabled(false);
                judgespecify_accept.setEnabled(true);
                judgespecify_deny.setEnabled(true);
                out_teacher1_name.setText(teacher.getName());
           }
            if (choosen_out_judges.size()==2) {
                outteacherslist.setEnabled(false);
                judgespecify_accept.setEnabled(true);
                judgespecify_deny.setEnabled(true);
                out_teacher1_name.setText(choosen_out_judges.get(choosen_out_judges.size()-1).getName());
                out_teacher2_name.setText(choosen_out_judges.get(choosen_out_judges.size()-2).getName());
            }
        }
    }//GEN-LAST:event_outteacher_saveMouseClicked

    private void judgespecify_acceptMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_judgespecify_acceptMouseClicked
        // TODO add your handling code here:        
        Request request = requests.get((String)waitingjudgereqslist.getSelectedItem());
        if (request.getStudent().getGraduate().equals("bach")) {
            set_inteacher(request, in_teacher.getID());
        }else if (request.getStudent().getGraduate().equals("post")) {
            set_inteacher(request, in_teacher.getID());
            set_outteacher(request, choosen_out_judges.get(choosen_out_judges.size()-1).getID());
        }else{
            set_inteacher(request, in_teacher.getID());
            set_outteacher(request, choosen_out_judges.get(choosen_out_judges.size()-2).getID(), choosen_out_judges.get(choosen_out_judges.size()-1).getID());
        }
        updateManager(request);
        //inteacherslist.removeItem((String)inteacherslist.getSelectedItem());
        waitingjudgereqslist.removeItem((String)waitingjudgereqslist.getSelectedItem());
        
    }//GEN-LAST:event_judgespecify_acceptMouseClicked

    private void observe_acceptActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_observe_acceptActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_observe_acceptActionPerformed
    
    public void Insert_Req_Observer_id(Request request , int observer_id){
        Connection con=null;
        try {
            String db_name="root";
            String db_pass="";
            String url="jdbc:mysql://localhost/maindb";
            Class.forName("com.mysql.jdbc.Driver").newInstance();
            con=DriverManager.getConnection(url,db_name,db_pass);
            Statement stmt2=con.createStatement();
            ResultSet res2=null;
            stmt2.executeUpdate("update requests set observerteacher_id='"+observer_id+"' where _id='"+request.get_id()+"'");
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    
    public void set_outteacher(Request request,int id1,int...id2){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id1+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingjudgelist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingjudgelist_id='"+str+"' where _id='"+id1+"'");
                }
                ResultSet resultSet = stmt2.executeQuery("select * from requests where _id='"+request.get_id()+"'");
                if (resultSet.next()) {
                    String str = resultSet.getString("outjudgeteachers_id");
                    str = find_and_input(parseString(str), id1);
                    stmt2.executeUpdate("update requests set outjudgeteachers_id='"+str+"' where _id='"+request.get_id()+"'");
                }
                
            }catch(Exception e){
                e.printStackTrace();
            }   
        if (id2!=null) {
            Connection con2=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con2=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con2.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id2[0]+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingjudgelist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingjudgelist_id='"+str+"' where _id='"+id2[0]+"'");
                }
                ResultSet resultSet = stmt2.executeQuery("select * from requests where _id='"+request.get_id()+"'");
                if (resultSet.next()) {
                    String str = resultSet.getString("outjudgeteachers_id");
                    str = find_and_input(parseString(str), id2[0]);
                    stmt2.executeUpdate("update requests set outjudgeteachers_id='"+str+"' where _id='"+request.get_id()+"'");
                }
                
            }catch(Exception e){
                e.printStackTrace();
            }   
        }
    }
    
    public void set_inteacher(Request request,int id){
        
         Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingjudgelist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingjudgelist_id='"+str+"' where _id='"+id+"'");
                }
                ResultSet resultSet = stmt2.executeQuery("select * from requests where _id='"+request.get_id()+"'");
                if (resultSet.next()) {
                    stmt2.executeUpdate("update requests set injudgeteacher_id='"+id+"' where _id='"+request.get_id()+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }                    
    }
    
    public Dateplace find_dateplace(int dateplace_id){
        Dateplace dateplace = null;
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from dateplace where _id='"+dateplace_id+"'");
                if (res2.next()) {
                    dateplace = new Dateplace(res2.getDate("date"), res2.getString("place"),true?res2.getInt("isfree")==1:false, res2.getInt("_id"));
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            return dateplace;
    }
    
    public void saveDetails(int teacher_id){
        try {
            //save a file
            FileWriter fileWriter = new FileWriter("teacher_sheet.txt");
            Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+teacher_id+"'");
                if (res2.next()) {
                    String name = (res2.getString("name"));
                    String gp = res2.getString("gp");
                    ArrayList<Integer> dateplace_list = parseString(res2.getString("dateplacelist_id"));
                    String stringBuffer = "";
                    for (int i = 0; i < dateplace_list.size(); i++) {
                        stringBuffer+=(find_dateplace(dateplace_list.get(i)).getdateplace()+"\n");
                    }
                    fileWriter.write(name+", "+gp+", "+stringBuffer);
                    fileWriter.close();
                }
            }catch(Exception e){
                e.printStackTrace();
            }
            
            
        } catch (IOException ex) {
            
        }
        
    }
    
    public void updateManager(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where (ismanager='"+1+"' AND gp='"+request.getStudent().getgp()+"')");
                if (res2.next()) {
                    int id = res2.getInt("_id");
                    String str = res2.getString("waitingmanagelist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingmanagelist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void updateJudge_id(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id ='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingjudgelist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingjudgelist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public void InsertIntoExpert(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from experttable");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverfind_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update experttable set waitingobserverfind_id='"+str+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }                    

    }
    
    public void deny_observation(Request request){
        //insert it into expert
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from experttable");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverfind_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update experttable set waitingobserverfind_id='"+str+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public void getInTeachers(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                System.out.println("dar getinteachers:"+request.getStudent().getgp());
                ResultSet res2 = stmt2.executeQuery("select * from teachers where gp='"+request.getStudent().getgp()+"'");
                int i =0;
                while (res2.next()) {
                    System.out.println("dar getinteachers...hanooz?");
                    Teacher teacher1 = new Teacher(res2.getString("name"), res2.getString("gp"), true?res2.getInt("ismanager")==1:false);
                    System.out.println(teacher1.name);
                    inteachers.put(i, teacher1);
                    middle1.put(teacher1.name, i);
                    i++;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public void getOutTeachers(Request request){
    Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                Integer i =0;
                ResultSet res2 = stmt2.executeQuery("select * from teachers where gp !='"+request.getStudent().getgp()+"'");
                while (res2.next()) {
                    Teacher teacher = new Teacher(res2.getString("name"), res2.getString("gp"), true?res2.getInt("ismanager")==1:false);
                    outteachers.put(i, teacher);
                    middle2.put(teacher.name, i);
                    i++;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void accept_guid_Req(Request request,boolean b){
        System.out.println("1-in accept");
        updateWaitingGuidList(request, current_guid_teacher_id);
         if (b) {
             System.out.println("2-in accept"+request.get_id());
            InsertMyGuidList(request, current_guid_teacher_id);
            InsertMyDateplace(request, current_guid_teacher_id);
            if (other_guid_teacher_id != -1) {
                if (isInOtherGuidList(request, other_guid_teacher_id)) {
                    updateManagerList(request);
                }
            }else{
                System.out.println("end of ACCEPT");
                updateManagerList(request);
            }
        }else{
            if (other_guid_teacher_id != -1) {
                if (isInOtherGuidList(request, other_guid_teacher_id)) {
                    deleteOtherGuidList(request, other_guid_teacher_id);
                    deleteOtherDateplace(request, other_guid_teacher_id);
                }else{
                    updateWaitingGuidList(request, other_guid_teacher_id);
                }
            }
             request_destruction(request);
         }
    }
    
    public void request_destruction(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from dateplace where _id='"+request.getdateplace().getId()+"'");
                if (res2.next()) {
                    stmt2.executeUpdate("update dateplace set isfree='"+1+"' where _id='"+res2.getInt("_id")+"'");
                }
                res2 = stmt2.executeQuery("select * from students where _id='"+request.getStudent().get_id()+"'");
                if (res2.next()) {
                    stmt2.executeUpdate("update students set request_id='"+null+"' where _id='"+res2.getInt("_id")+"'");
                }
                stmt2.executeUpdate("delete from requests where _id='"+request.get_id()+"'");
                
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public boolean isInOtherGuidList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("myguidlist_id");
                    boolean b = canFind(parseString(str), request.get_id());
                    if (b) {
                        return true;
                    }
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        return false;
    }
    
    public boolean isInOtherJudgeList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                
                ArrayList<Integer> _ids = parseString(request.getOutjudgeteachers_id());
                _ids.add(request.getInjudgeteacher_id());
                int a = 0;
                for (int i = 0; i < _ids.size(); i++) {
                    Statement stmt2=con.createStatement();
                    ResultSet res2=null;
                    res2 = stmt2.executeQuery("select * from teachers where _id='"+_ids.get(i)+"'");
                    if (res2.next()) {
                        String str = res2.getString("myjudgelist_id");
                        boolean b = canFind(parseString(str), request.get_id());
                        if (b) {
                            a++;
                        }
                    }
                }
                if (a == _ids.size()) {
                    return true;
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        return false;
    }
    
    public boolean canFind(ArrayList<Integer> arr , int id){
        ArrayList<Integer> arrayList = new ArrayList<>();
        arrayList = arr;
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                if (id == arrayList.get(i)) {
                    return true;
                }
                
            }
        } catch (Exception e) {
        }
        return false;
    }
    
    public void deleteOtherGuidList(Request request, int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("dateplacelist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set dateplacelist_id='"+str+"' where _id='"+id+"'");
                    
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    
    }
    
    public void updateManagerList(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where (ismanager='"+1+"' AND gp='"+request.getStudent().getgp()+"')");
                if (res2.next()) {
                    int id = res2.getInt("_id");
                    String str = res2.getString("waitingmanagelist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingmanagelist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public void deleteOtherDateplace(Request request, int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("dateplacelist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set dateplacelist_id='"+str+"' where _id='"+id+"'");
                    
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public void InsertMyDateplace(Request request,int teacher_id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+teacher_id+"'");
                if (res2.next()) {
                    String str = res2.getString("dateplacelist_id");
                    str = find_and_input(parseString(str), request.getdateplace().getId());
                    stmt2.executeUpdate("update teachers set dateplacelist_id='"+str+"' where _id='"+teacher_id+"'");
                    
                }
            }catch(Exception e){
                e.printStackTrace();
            }
        
    }
    
    public void UpdateState(Request request){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from requests where _id='"+request.get_id()+"'");
                if (res2.next()) {
                    stmt2.executeUpdate("update teachers set myjudgelist_id='"+"accepted"+"' where _id='"+request.get_id()+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void InsertMyJudgeList(Request request, int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("myjudgelist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set myjudgelist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void InsertMyGuidList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("myguidlist_id");
                    System.out.println("req _id:"+request.get_id());
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set myguidlist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void InsertMyObserveList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("myobservelist_id");
                    str = find_and_input(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set myobservelist_id='"+str+"' where _id='"+id+"'");
                }
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public String find_and_input(ArrayList<Integer> arr,int _id){
        String answer = "";
        try {
            ArrayList <Integer> arrayList = new ArrayList<>();
            arrayList = arr;
            arrayList.add(_id);

            for (int i = 0; i < arrayList.size(); i++) {
                answer +=Integer.toString(arrayList.get(i))+",";
            }
        } catch (Exception e) {
        }
        return answer;
        
    }
    
    public void updateWaitingGuidList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingguidlist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingguidlist_id='"+str+"' where _id='"+id+"'");
                    
                }
                
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void updateWaitingJudgeList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingjudgelist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingjudgelist_id='"+str+"' where _id='"+id+"'");
                }                
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public void updateWaitingObserveList(Request request,int id){
        Connection con=null;
            try {
                String db_name="root";
                String db_pass="";
                String url="jdbc:mysql://localhost/maindb";
                Class.forName("com.mysql.jdbc.Driver").newInstance();
                con=DriverManager.getConnection(url,db_name,db_pass);
                Statement stmt2=con.createStatement();
                ResultSet res2=null;
                res2 = stmt2.executeQuery("select * from teachers where _id='"+id+"'");
                if (res2.next()) {
                    String str = res2.getString("waitingobserverlist_id");
                    str = find_and_delete(parseString(str), request.get_id());
                    stmt2.executeUpdate("update teachers set waitingobserverlist_id='"+str+"' where _id='"+id+"'");
                }                
            }catch(Exception e){
                e.printStackTrace();
            }
    }
    
    public String find_and_delete(ArrayList<Integer> arr,int _id){
        String answer = "";
        try {
            ArrayList <Integer> arrayList = new ArrayList<>();
            arrayList = arr;
            arrayList.remove(_id);

            for (int i = 0; i < arrayList.size(); i++) {
                answer +=Integer.toString(arrayList.get(i))+",";
            }
        } catch (Exception e) {
        }
        return answer;
    }
    
    public ArrayList<Integer> parseString(String str){
        ArrayList<Integer> arrayList = new ArrayList<>();
        try{
            String s[] = str.split(",");
            for (int i = 0; i < s.length; i++) {
                arrayList.add(Integer.parseInt(s[i]));
            }
        }catch(Exception e){}
        return arrayList;
    }
    
    
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(teacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(teacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(teacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(teacherPage.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new teacherPage().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton accept;
    private javax.swing.JButton deny;
    private javax.swing.JButton exit;
    private javax.swing.JComboBox<String> guidwaitinglist;
    private javax.swing.JLabel in_teacher_name;
    private javax.swing.JButton inteacher_save;
    private javax.swing.JComboBox<String> inteacherslist;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JButton judge_accept;
    private javax.swing.JButton judge_deny;
    private javax.swing.JLabel judge_txt_dateplace;
    private javax.swing.JLabel judge_txt_student_name;
    private javax.swing.JButton judgespecify_accept;
    private javax.swing.JButton judgespecify_deny;
    private javax.swing.JComboBox<String> judgewaitinglist;
    private javax.swing.JButton observe_accept;
    private javax.swing.JButton observe_deny;
    private javax.swing.JLabel observe_txt_dateplace;
    private javax.swing.JLabel observe_txt_student_name;
    private javax.swing.JComboBox<String> observewaitinglist;
    private javax.swing.JLabel out_teacher1_name;
    private javax.swing.JLabel out_teacher2_name;
    private javax.swing.JButton outteacher_save;
    private javax.swing.JComboBox<String> outteacherslist;
    private javax.swing.JTabbedPane tabPane;
    private javax.swing.JLabel txt_dateplace;
    private javax.swing.JLabel txt_student_name;
    private javax.swing.JComboBox<String> waitingjudgereqslist;
    // End of variables declaration//GEN-END:variables
}
