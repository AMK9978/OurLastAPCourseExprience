/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication64;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author Launcher
 */
@Entity
@Table(name = "teachers", catalog = "maindb", schema = "")
@NamedQueries({
    @NamedQuery(name = "Teachers.findAll", query = "SELECT t FROM Teachers t")
    , @NamedQuery(name = "Teachers.findById", query = "SELECT t FROM Teachers t WHERE t.id = :id")
    , @NamedQuery(name = "Teachers.findByGp", query = "SELECT t FROM Teachers t WHERE t.gp = :gp")
    , @NamedQuery(name = "Teachers.findByIsmanager", query = "SELECT t FROM Teachers t WHERE t.ismanager = :ismanager")})
public class Teachers implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "_id")
    private Integer id;
    @Basic(optional = false)
    @Lob
    @Column(name = "name")
    private String name;
    @Basic(optional = false)
    @Column(name = "gp")
    private String gp;
    @Basic(optional = false)
    @Lob
    @Column(name = "email")
    private String email;
    @Basic(optional = false)
    @Lob
    @Column(name = "password")
    private String password;
    @Basic(optional = false)
    @Column(name = "ismanager")
    private boolean ismanager;
    @Lob
    @Column(name = "waitingguidelist_id")
    private String waitingguidelistId;
    @Lob
    @Column(name = "myguidelist_id")
    private String myguidelistId;
    @Lob
    @Column(name = "waitingmanagelist_id")
    private String waitingmanagelistId;
    @Lob
    @Column(name = "waitingjudgelist_id")
    private String waitingjudgelistId;
    @Lob
    @Column(name = "myjudgelist_id")
    private String myjudgelistId;
    @Lob
    @Column(name = "waitingobservelist_id")
    private String waitingobservelistId;
    @Lob
    @Column(name = "myobservelist_id")
    private String myobservelistId;
    @Lob
    @Column(name = "dateplace_id")
    private String dateplaceId;

    public Teachers() {
    }

    public Teachers(Integer id) {
        this.id = id;
    }

    public Teachers(Integer id, String name, String gp, String email, String password, boolean ismanager) {
        this.id = id;
        this.name = name;
        this.gp = gp;
        this.email = email;
        this.password = password;
        this.ismanager = ismanager;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        Integer oldId = this.id;
        this.id = id;
        changeSupport.firePropertyChange("id", oldId, id);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        String oldName = this.name;
        this.name = name;
        changeSupport.firePropertyChange("name", oldName, name);
    }

    public String getGp() {
        return gp;
    }

    public void setGp(String gp) {
        String oldGp = this.gp;
        this.gp = gp;
        changeSupport.firePropertyChange("gp", oldGp, gp);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        String oldEmail = this.email;
        this.email = email;
        changeSupport.firePropertyChange("email", oldEmail, email);
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        String oldPassword = this.password;
        this.password = password;
        changeSupport.firePropertyChange("password", oldPassword, password);
    }

    public boolean getIsmanager() {
        return ismanager;
    }

    public void setIsmanager(boolean ismanager) {
        boolean oldIsmanager = this.ismanager;
        this.ismanager = ismanager;
        changeSupport.firePropertyChange("ismanager", oldIsmanager, ismanager);
    }

    public String getWaitingguidelistId() {
        return waitingguidelistId;
    }

    public void setWaitingguidelistId(String waitingguidelistId) {
        String oldWaitingguidelistId = this.waitingguidelistId;
        this.waitingguidelistId = waitingguidelistId;
        changeSupport.firePropertyChange("waitingguidelistId", oldWaitingguidelistId, waitingguidelistId);
    }

    public String getMyguidelistId() {
        return myguidelistId;
    }

    public void setMyguidelistId(String myguidelistId) {
        String oldMyguidelistId = this.myguidelistId;
        this.myguidelistId = myguidelistId;
        changeSupport.firePropertyChange("myguidelistId", oldMyguidelistId, myguidelistId);
    }

    public String getWaitingmanagelistId() {
        return waitingmanagelistId;
    }

    public void setWaitingmanagelistId(String waitingmanagelistId) {
        String oldWaitingmanagelistId = this.waitingmanagelistId;
        this.waitingmanagelistId = waitingmanagelistId;
        changeSupport.firePropertyChange("waitingmanagelistId", oldWaitingmanagelistId, waitingmanagelistId);
    }

    public String getWaitingjudgelistId() {
        return waitingjudgelistId;
    }

    public void setWaitingjudgelistId(String waitingjudgelistId) {
        String oldWaitingjudgelistId = this.waitingjudgelistId;
        this.waitingjudgelistId = waitingjudgelistId;
        changeSupport.firePropertyChange("waitingjudgelistId", oldWaitingjudgelistId, waitingjudgelistId);
    }

    public String getMyjudgelistId() {
        return myjudgelistId;
    }

    public void setMyjudgelistId(String myjudgelistId) {
        String oldMyjudgelistId = this.myjudgelistId;
        this.myjudgelistId = myjudgelistId;
        changeSupport.firePropertyChange("myjudgelistId", oldMyjudgelistId, myjudgelistId);
    }

    public String getWaitingobservelistId() {
        return waitingobservelistId;
    }

    public void setWaitingobservelistId(String waitingobservelistId) {
        String oldWaitingobservelistId = this.waitingobservelistId;
        this.waitingobservelistId = waitingobservelistId;
        changeSupport.firePropertyChange("waitingobservelistId", oldWaitingobservelistId, waitingobservelistId);
    }

    public String getMyobservelistId() {
        return myobservelistId;
    }

    public void setMyobservelistId(String myobservelistId) {
        String oldMyobservelistId = this.myobservelistId;
        this.myobservelistId = myobservelistId;
        changeSupport.firePropertyChange("myobservelistId", oldMyobservelistId, myobservelistId);
    }

    public String getDateplaceId() {
        return dateplaceId;
    }

    public void setDateplaceId(String dateplaceId) {
        String oldDateplaceId = this.dateplaceId;
        this.dateplaceId = dateplaceId;
        changeSupport.firePropertyChange("dateplaceId", oldDateplaceId, dateplaceId);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Teachers)) {
            return false;
        }
        Teachers other = (Teachers) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "javaapplication64.Teachers[ id=" + id + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
